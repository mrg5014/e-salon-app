import "reflect-metadata";
import express, { Request, Response } from "express";
import { createConnection } from "typeorm";
import { User } from "./entity/User";
import { json, urlencoded } from "body-parser";

const app = express();

createConnection()
  .then(async (connection) => {
    console.log("Inserting a new user into the database...");

    app.use(json({ limit: "30mb", type: "json" }));
    app.use(urlencoded({ limit: "30mb", extended: true }));

    app.get("/", async (req: Request, res: Response) => {
      const users = await connection.manager.find(User);
      res.send(users);
    });

    const { PORT } = process.env;

    app.listen(PORT, () =>
      console.log("Server listening on port: ", PORT)
    );
  })
  .catch((error) => console.log(error));
